import crypto from 'crypto';

export function generateValidationHash(key: string, webguid: string, State: string) {
  const hash = crypto.createHash('sha512');
  const data = hash.update(key + webguid + State, 'utf-8');
  const gen_hash = data.digest('hex');

  return gen_hash;
}

export function generatePaymentHash(key: string, webguid: string, State: string, Amount: string) {
  const ishash = crypto.createHash('sha512');
  const data = ishash.update(key + webguid + State + Amount, 'utf-8');
  const gen_hash = data.digest('hex');

  return gen_hash;
}
