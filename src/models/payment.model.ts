

export interface PaymentInput {
  Webguid: string;
  AmountPaid: string;
  PaymentRef: string;
  CreditAccount: string;
  Date: Date | string;
//   Hash: string;
  State: string;
  ClientId: number;
  PaymentChannel: string;
  TellerName: string;
  TellerID: string;
  BankNote: string;
}
