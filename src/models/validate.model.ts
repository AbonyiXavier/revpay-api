
export interface ValidateInput {
  Webguid: string;
  Date?: Date | string;
  // Hash: string;
  State: string;
  Clientid: number;
  TellerID?: string;
  Currency: string;
  Type: string;
}

