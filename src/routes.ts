import { Express, Request, Response } from 'express';
import { validateReferenceHandler } from './controller/validate.controller';
import { paymentNotificationHandler } from './controller/payment.controller';
import validateResource from './middleware/validateResource';
import { createValidateSchema } from './schema/validateSchema';
import { createPaymentSchema } from './schema/paymentSchema';

function routes(app: Express) {
  app.get('/', (req: Request, res: Response) => res.sendStatus(200));

  app.post('/api/validate', validateResource(createValidateSchema), validateReferenceHandler);

  app.post('/api/payment', validateResource(createPaymentSchema), paymentNotificationHandler);
}

export default routes;
