import { object, string, number, date, TypeOf } from 'zod';

export const createValidateSchema = object({
  body: object({
    Webguid: string({
      required_error: 'Webguid is required',
    }),
    Date: string().optional(),
    // Clientid: number({
    //   required_error: 'Client id is required',
    // }),
    // TellerID: string({
    //   required_error: 'Teller id is required',
    // })
  }),
});

export type CreateValidateInput = TypeOf<typeof createValidateSchema>;
