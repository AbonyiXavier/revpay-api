import { object, string, number, date, TypeOf } from 'zod';

export const createPaymentSchema = object({
  body: object({
    Webguid: string({
      required_error: 'Webguid is required',
    }),
    AmountPaid: string({
      required_error: 'Amount Paid is required',
    }),
    PaymentRef: string({
      required_error: 'Payment Ref is required',
    }),
    CreditAccount: string({
      required_error: 'Credit Account is required',
    }),
    Date: string({
      required_error: 'Date is required',
    }),
    ClientId: number({
      required_error: 'Client id is required',
    }),
    PaymentChannel: string({
      required_error: 'Payment Channel is required',
    }),
    TellerName: string({
      required_error: 'Teller Name is required',
    }),
    TellerID: string({
      required_error: 'Teller id is required',
    }),
    BankNote: string({
      required_error: 'Bank Note is required',
    }),
  }),
});

export type CreatePaymentInput = TypeOf<typeof createPaymentSchema>;
