import axios from 'axios';
import config from 'config';
import { ValidateInput } from '../models/validate.model';

// const validateUrl = config.get<string>('validateTestAPI');

export function validateReference(input: ValidateInput): Promise<any> {
  return new Promise((resolve, reject) => {
    axios
      .post(`${process.env.BaseUrl}/Interface/Validate`, input)
      .then((response) => {
        resolve(response);
      })
      .catch((err) => {
        console.log('error validate', err);
        reject(err);
      });
  });
}
