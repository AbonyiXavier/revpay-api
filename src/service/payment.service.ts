import axios from 'axios';
import config from 'config';
import { PaymentInput } from '../models/payment.model';

// const paymentUrl = config.get<string>('paymentTestAPI');

export function paymentNotification(input: PaymentInput): Promise<any> {
  return new Promise((resolve, reject) => {
    axios
      .post(`${process.env.BaseUrl}/Interface/Payment`, input)
      .then((response) => {
        resolve(response);
      })
      .catch((err) => {
        console.log('error payment', err);

        reject(err);
      });
  });
}
