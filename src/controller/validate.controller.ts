import { Request, Response } from 'express';
import config from 'config';
import { validateReference } from '../service/validate.service';
import { generateValidationHash } from '../utils/generateHash';
import { CreateValidateInput } from '../schema/validateSchema';
import logger from '../utils/logger';
import { parseInt } from 'lodash';
require('dotenv').config();

// Key from config file
// const key = config.get<string>('key');

const key = <string>process.env.key;
const state = <string>process.env.stateTest;
const cliendId = parseInt(<string>process.env.TestClientID);
console.log('heree');

export async function validateReferenceHandler(
  req: Request<{}, {}, CreateValidateInput['body']>,
  res: Response
) {
  try {
    let { Webguid, Date } = req.body;

    const hashValue = generateValidationHash(key, Webguid, state);

    const data = {
      Webguid,
      Date,
      Hash: hashValue,
      State: state,
      Clientid: cliendId,
      Currency: 'NGN',
      Type: 'WEBGUID',
    };

    const ref = await validateReference(data);

    return res.send({ message: 'Reference Validated Successfully', data: ref.data });
  } catch (error: any) {
    logger.error(error);
    return res.status(500).send(error.message);
  }
}
