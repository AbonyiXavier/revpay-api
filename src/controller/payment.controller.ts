import { Request, Response } from 'express';
import config from 'config';
import { paymentNotification } from '../service/payment.service';
import { generatePaymentHash } from '../utils/generateHash';
import { CreatePaymentInput } from '../schema/paymentSchema';
import logger from '../utils/logger';
require('dotenv').config();

// Key from config file
// const key = config.get<string>('key');

const key = <string>process.env.key;
const state = <string>process.env.stateTest;
const cliendId = parseInt(<string>process.env.TestClientID);

export async function paymentNotificationHandler(
  req: Request<{}, {}, CreatePaymentInput['body']>,
  res: Response
) {
  try {
    let {
      Webguid,
      AmountPaid,
      PaymentRef,
      CreditAccount,
      Date,
      PaymentChannel,
      TellerName,
      TellerID,
      BankNote,
    } = req.body;

    const hashValue = generatePaymentHash(key, Webguid, state, AmountPaid);

    const data = {
      Webguid,
      AmountPaid,
      PaymentRef,
      CreditAccount,
      Date,
      Hash: hashValue,
      State: state,
      ClientId: cliendId,
      PaymentChannel,
      TellerName,
      TellerID,
      BankNote,
    };

    const ref = await paymentNotification(data);

    return res.send({ message: 'Payment was  Successfully', data: ref.data });
  } catch (error: any) {
    logger.error(error);
    return res.status(500).send(error.message);
  }
}
