import express from 'express';
import config from 'config';
import logger from './utils/logger';
import routes from './routes';
import http from 'http';
import { Request, Response } from 'express';

require('dotenv').config();

// getting the PORT from config file
// const port = config.get<number>('port');

const port = process.env.PORT;
// const port = process.env.PORT || 1338;

const app = express();

app.use(express.json());

app.get('/api/v1', (req: Request, res: Response) => {
  res.status(200).json({
    status: 'success',
    message: 'Ojudo API',
  });
});

const server = http.createServer(app);

server.listen(port, async () => {
  logger.info(`App is running at http://localhost:${port}`);

  routes(app);
});
